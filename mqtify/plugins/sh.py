import subprocess

from yapsy.IPlugin import IPlugin
from yapsy.PluginManager import PluginManagerSingleton


class sh(IPlugin):
    '''Shell plugin.

    Execute shell commands on host.
    '''
    CONF_SECTION = 'Shell'
    TOPIC_SUB = '/mqtify/sh/run'
    WHITELIST = None

    def __init__(self):
        super().__init__()
        self._setup_mqtify_attr()
        self._setup_conf()

    def _setup_mqtify_attr(self):
        '''Get attributes from Mqtify using manager singleton.'''
        manager = PluginManagerSingleton.get()
        self.mqtt = manager.mqtify.mqtt
        self.conf = manager.mqtify.conf

    def _setup_conf(self):
        '''Read configuration.'''
        if self.CONF_SECTION in self.conf:
            conf = self.conf[self.CONF_SECTION]
            if 'topic_sub' in conf:
                self.TOPIC_SUB = str(conf['topic_sub'])
            if 'whitelist' in conf:
                self.WHITELIST = str(conf['whitelist']).split(';;')

    def on_connect(self, client, userdata, flags, rc):
        '''Called when the broker responds to our connection request.'''
        client.subscribe(self.TOPIC_SUB)

    def on_message(self, client, userdata, msg):
        '''Called when a message has been received.'''
        if msg.topic == self.TOPIC_SUB:
            payload = bytes.decode(msg.payload)
            cmd = payload.split()[0]

            if self.WHITELIST and cmd in self.WHITELIST:
                subprocess.Popen(payload, shell=True)
            elif not self.WHITELIST:
                subprocess.Popen(payload, shell=True)
