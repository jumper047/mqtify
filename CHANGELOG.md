# Change Log

## [v0.3.3](https://github.com/envolyse/mqtify/tree/v0.3.3) (2017-12-28)

[Full Changelog](https://github.com/envolyse/mqtify/compare/v0.3.2...v0.3.3)

**What's new:**

- Audio plugin enhancements [\#22](https://github.com/envolyse/mqtify/issues/22)
- CRD plugin enhancements [\#21](https://github.com/envolyse/mqtify/issues/21)
- Fullscreen sensor plugin [\#15](https://github.com/envolyse/mqtify/issues/15)

## [v0.3.2](https://github.com/envolyse/mqtify/tree/v0.3.2) (2017-12-26)

[Full Changelog](https://github.com/envolyse/mqtify/compare/v0.3.1...v0.3.2)

**What's new:**

- Documentation structure enhancements [\#20](https://github.com/envolyse/mqtify/issues/20)
- Install plugin dependencies using setuptools

## [v0.3.1](https://github.com/envolyse/mqtify/tree/v0.3.1) (2017-12-25)
[Full Changelog](https://github.com/envolyse/mqtify/compare/v0.3...v0.3.1)

**What's new:**

- PulseAudio plugin [\#19](https://github.com/envolyse/mqtify/issues/19)
- Removed i3lock plugin [\#16](https://github.com/envolyse/mqtify/issues/16)
- AlsaAudio plugin enhancements [\#14](https://github.com/envolyse/mqtify/issues/14)
- Shell plugin whitelist commands [\#13](https://github.com/envolyse/mqtify/issues/13)
- Consolidate description. [\#18](https://github.com/envolyse/mqtify/pull/18) ([mike402](https://github.com/mike402))

**Fixed bugs:**

- CRD plugin: round values [\#17](https://github.com/envolyse/mqtify/issues/17)

## [v0.3](https://github.com/envolyse/mqtify/tree/v0.3) (2017-12-25)

[Full Changelog](https://github.com/envolyse/mqtify/compare/v0.2.3...v0.3)

**What's new:**

- Argument parser [\#12](https://github.com/envolyse/mqtify/issues/12)
- Sphinx documentation [\#7](https://github.com/envolyse/mqtify/issues/7)
- Wiki [\#6](https://github.com/envolyse/mqtify/issues/6)
- New plugins [\#5](https://github.com/envolyse/mqtify/issues/5)

## [v0.2.3](https://github.com/envolyse/mqtify/tree/v0.2.3) (2017-12-21)
[Full Changelog](https://github.com/envolyse/mqtify/compare/v0.2.2...v0.2.3)

**What's new:**

- SSL/TLS support [\#8](https://github.com/envolyse/mqtify/issues/8)

## [v0.2.2](https://github.com/envolyse/mqtify/tree/v0.2.2) (2017-12-21)

**What's new:**

- Add auth parameters for MQTT instance, so that user could connect to server using password [\#3](https://github.com/envolyse/mqtify/issues/3)

**Fixed bugs:**

- Can't activate a plugin that have a space in it's name [\#10](https://github.com/envolyse/mqtify/issues/10)
- Plugins do not subscribe to their topic [\#2](https://github.com/envolyse/mqtify/issues/2)
