# Mqtify [![Documentation Status](https://readthedocs.org/projects/mqtify/badge/?version=latest)](http://mqtify.readthedocs.io/) [![PyPI version](https://badge.fury.io/py/mqtify.svg)](https://badge.fury.io/py/mqtify)
Mqtify is a modular tool helping you automate your smart home by connecting Unix devices to MQTT broker. The following features are included by default:

- [Send availability status](http://mqtify.readthedocs.io/page/plugins/availability.html)
- [Turn your screen off](http://mqtify.readthedocs.io/page/plugins/dpms.html)
- [Lock the screen](http://mqtify.readthedocs.io/page/plugins/sh.html)
- [Turn up the volume](http://mqtify.readthedocs.io/page/plugins/alsaaudio.html)
- [Receive CPU, Disk, and memory stats](http://mqtify.readthedocs.io/page/plugins/crd.html)
- [Push notifications](http://mqtify.readthedocs.io/page/plugins/sh.html)
- More to come

All features are implemented as [plugins](http://yapsy.sourceforge.net/). If you are interested in developing a plugin, see [this guide](http://mqtify.readthedocs.io/page/guides/developing_a_plugin.html).

# Screenshots
Example setup (as seen in home assistant):

![Home Assistant](https://raw.githubusercontent.com/envolyse/mqtify/master/docs/screenshot.png)

# Installation
Install using pip:
```shell
% pip install mqtify
```
or, to install locally:
```shell
% pip install --user mqtify
```

Check out [configuration](http://mqtify.readthedocs.io/page/guides/configuration.html) and [available plugins](http://mqtify.readthedocs.io/page/plugins/index.html)!
