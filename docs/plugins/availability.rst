===================
Availability plugin
===================
Publishes availability updates.

***************
Payload
***************
You can receive these payloads from ``topic_pub``:

- ``OFF`` - Mqtify is dead :(
- ``ON`` - Mqtify is running

***************
Configuration
***************

.. code-block:: ini

    [Availability]
    # Topic to publush
    topic_pub = /mqtify/availability

    # Topic to subscribe
    topic_sub = /mqtify/#

    # Time to wait before next publish
    timeout = 30
