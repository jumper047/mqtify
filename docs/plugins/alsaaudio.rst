================
AlsaAudio plugin
================
Control AlsaAudio from remote

***************
Payload
***************
You can send these payloads to ``topic_sub``:

- ``OFF`` - Mutes AlsaAudio
- ``ON`` - Unmutes AlsaAudio
- ``TURN_UP`` - Turn up volume
- ``TURN_DOWN`` - Turn down volume
- **Integer** from 0 to 100 - Set exact volume level

You can receive these payloads from ``topic_pub_sound``:

- ``OFF`` - AlsaAudio is muted
- ``ON`` - AlsaAudio is unmuted

You can receive **volume level** in percents from ``topic_pub_vol``.
  
***************
Dependencies
***************
This plugin depends on pyalsaaudio_. You can install it using pip:

- ``pip install 'mqtify[alsaaudio]'``

.. _pyalsaaudio: https://larsimmisch.github.io/pyalsaaudio/

***************
Configuration
***************

.. code-block:: ini

    [AlsaAudio]
    # Topic to publish volume level updates
    topic_pub_vol = /mqtify/alsaaudio/vol
    
    # Topic to publish mute updates
    topic_pub_sound = /mqtify/alsaaudio/sound

    # Topic to subscribe
    topic_sub = /mqtify/alsaaudio/set

    # Time to wait before next publish
    timeout = 5

    # Channel ID
    ch_id = 0
